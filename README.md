#GRBL fork - firmware for laseraxe board

Arduino nano on laseraxe board uses modified pin mapping.

This is a fork of [gnea/grbl](https://github.com/gnea/grbl) - click to view the original Grbl project.


###Laseraxe board pin map

        Z_STEP_BIT      PD7 // Nano Digital Pin 7
        Z_DIRECTION_BIT PD6 // Nano Digital Pin 6
        Y_STEP_BIT      PD5 // Nano Digital Pin 5
        Y_DIRECTION_BIT PD4 // Nano Digital Pin 4
        X_STEP_BIT      PD3 // Nano Digital Pin 3
        X_DIRECTION_BIT PD2 // Nano Digital Pin 2

        Z_LIMIT_BIT     PB4 // Nano Digital Pin 12
        Y_LIMIT_BIT     PB2 // Nano Digital Pin 10
        X_LIMIT_BIT     PB1 // Nano Digital Pin 9

        SPINDLE_ENABLE_BIT PB3  // Nano Digital Pin 11
                                // TTL+ (via variable resistor and emitter follower)
                                // LASER- BU406 (NPN) (3 in parallel) collector (via variable resistor and optocoupler)

        COOLANT_FLOOD_BIT PC3   // Nano Analog Pin 3
                                // COOL- 2SD882 (NPN) collector (via optocoupler)

        DCOUT+                  // 12 V power



###Original firmware - settings

        Grbl 0.8 ['$' for help]
        F300S1000{0/0}
        [0.8.2015:]

        $0=10 (step pulse, usec)
        $1=255 (step idle delay, msec)
        $2=0 (step port invert mask:00000000)
        $3=7 (dir port invert mask:00000111)
        $4=0 (step enable invert, bool)
        $5=0 (limit pins invert, bool)
        $6=0 (probe pin invert, bool)
        $10=3 (status report mask:00000011)
        $11=0.010 (junction deviation, mm)
        $12=0.002 (arc tolerance, mm)
        $13=0 (report inches, bool)
        $20=0 (soft limits, bool)
        $21=0 (hard limits, bool)
        $22=0 (homing cycle, bool)
        $23=0 (homing dir invert mask:00000000)
        $24=25.000 (homing feed, mm/min)
        $25=500.000 (homing seek, mm/min)
        $26=250 (homing debounce, msec)
        $27=1.000 (homing pull-off, mm)
        $100=400.000 (x, step/mm)
        $101=400.000 (y, step/mm)
        $102=400.000 (z, step/mm)
        $110=1500.000 (x max rate, mm/min)
        $111=1500.000 (y max rate, mm/min)
        $112=1500.000 (z max rate, mm/min)
        $120=1000.000 (x accel, mm/sec^2)
        $121=1000.000 (y accel, mm/sec^2)
        $122=1000.000 (z accel, mm/sec^2)
        $130=200.000 (x max travel, mm)
        $131=200.000 (y max travel, mm)
        $132=200.000 (z max travel, mm)

        $N0=F300S1000
        $N1=


###Original firmware - restore from backup

        Fuses OK (E:FD, H:DA, L:FF)
        avrdude -p m328p -U lfuse:w:0xff:m -U hfuse:w:0xda:m -U efuse:w:0xfd:m
        avrdude -p m328p -U flash:w:grbl_0.8_laseraxe_orig_flash.hex -U eeprom:w:grbl_0.8_laseraxe_orig_eeprom.hex
